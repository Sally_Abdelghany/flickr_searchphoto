/**
 * 
 */
package oms.interview.flickr;

import java.util.ArrayList;

import oms.interview.flickr.adapter.PhotoFullScreenAdapter;
import oms.interview.flickr.presenter.PhotoViewerPresenter;
import oms.interview.flickr.services.model.photoItem;
import oms.interview.flickr.util.DepthPageTransformer;
import oms.interview.flickr.util.constants;
import oms.interview.flickr.view.IPhotoViewerView;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

/**
 * @author Sally
 *
 */
public class PhotoViewerActivity extends Activity implements IPhotoViewerView {

	private PhotoViewerPresenter photoViewerPresenter = new PhotoViewerPresenter();
	private ArrayList<photoItem> PhotosList;
	private int selectedID;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_photoviewer);

		overridePendingTransition(R.anim.fadein, R.anim.fadeout);

		photoViewerPresenter.setView(this);

		selectedID = getIntent().getIntExtra(constants.SELECTED_PHOTO_KEY, -1);
		PhotosList = (ArrayList<photoItem>) getIntent().getSerializableExtra(
				constants.PHOTOS_LIST_KEY);

		if (PhotosList != null) {
			ViewPager viewPager = (ViewPager) findViewById(R.id.vpPhotoViewer);
			PhotoFullScreenAdapter adapter = new PhotoFullScreenAdapter(
					getApplicationContext(), PhotosList);
			viewPager.setAdapter(adapter);
			viewPager.setCurrentItem(selectedID);
			viewPager.setPageTransformer(true, new DepthPageTransformer());
		}
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return getApplicationContext();
	}
}
