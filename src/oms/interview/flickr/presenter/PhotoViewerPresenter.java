/**
 * 
 */
package oms.interview.flickr.presenter;

import oms.interview.flickr.services.INetworkService;
import oms.interview.flickr.services.implementation.NetworkService;
import oms.interview.flickr.view.IPhotoViewerView;

/**
 * @author Sally
 *
 */
public class PhotoViewerPresenter {
	private INetworkService photoNetworkService;
	private IPhotoViewerView photoView;

	public PhotoViewerPresenter() {
		this.photoNetworkService = new NetworkService();
	}

	public PhotoViewerPresenter(INetworkService networkService) {
		this.photoNetworkService = networkService;
	}

	public void setView(IPhotoViewerView view) {
		this.photoView = view;
	}
}
