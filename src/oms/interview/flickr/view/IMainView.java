/**
 * 
 */
package oms.interview.flickr.view;

import java.util.ArrayList;

import oms.interview.flickr.services.model.photoItem;
import android.content.Context;

/**
 * @author Sally
 *
 */
public interface IMainView {
	Context getContext();

	void showProgress();

	void hideProgress();

	void showPageError();

	/**
	 * @param photosList
	 */
	void setPhotosGrid(ArrayList<photoItem> photosList);
}
